#!/bin/bash
for f in ./FeatureFiles/MoodableEMU/176FeaturesSCB/FeatureSelected*clip*.csv; do echo 
echo "Processing $f file.."; 
sbatch ./mljob.sh $f 5 -1 down moodableemu_folds.json
done
for f in ./FeatureFiles/MoodableEMU/176FeaturesSCB/FeatureSelected*clip*.csv; do echo 
echo "Processing $f file.."; 
sbatch ./mljob.sh $f 5 -1 up moodableemu_folds.json
done
for f in ./FeatureFiles/MoodableEMU/176FeaturesSCB/FeatureSelected*clip*.csv; do echo 
echo "Processing $f file.."; 
sbatch ./mljob.sh $f 5 -1 None moodableemu_folds.json
done
