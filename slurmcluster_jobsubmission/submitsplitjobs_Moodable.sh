#!/bin/bash
sbatch splitjob.sh './Moodable/original/*.wav' ./split.sh 2 0 ./Moodable/2secOver0
sbatch splitjob.sh './Moodable/original/*.wav' ./split.sh 2 0.25 ./Moodable/2secOver0.25
sbatch splitjob.sh './Moodable/original/*.wav' ./split.sh 2 0.5 ./Moodable/2secOver0.5
sbatch splitjob.sh './Moodable/original/*.wav' ./split.sh 2 0.75 ./Moodable/2secOver0.75
sbatch splitjob.sh './Moodable/original/*.wav' ./split.sh 3 0 ./Moodable/3secOver0
sbatch splitjob.sh './Moodable/original/*.wav' ./split.sh 3 0.25 ./Moodable/3secOver0.25
sbatch splitjob.sh './Moodable/original/*.wav' ./split.sh 3 0.5 ./Moodable/3secOver0.5
sbatch splitjob.sh './Moodable/original/*.wav' ./split.sh 3 0.75 ./Moodable/3secOver0.75
