#!/bin/bash
sbatch splitjob.sh './EMUOpen/original/*.wav' ./split.sh 2 0 ./EMUOpen/2secOver0
sbatch splitjob.sh './EMUOpen/original/*.wav' ./split.sh 2 0.25 ./EMUOpen/2secOver0.25
sbatch splitjob.sh './EMUOpen/original/*.wav' ./split.sh 2 0.5 ./EMUOpen/2secOver0.5
sbatch splitjob.sh './EMUOpen/original/*.wav' ./split.sh 2 0.75 ./EMUOpen/2secOver0.75
sbatch splitjob.sh './EMUOpen/original/*.wav' ./split.sh 3 0 ./EMUOpen/3secOver0
sbatch splitjob.sh './EMUOpen/original/*.wav' ./split.sh 3 0.25 ./EMUOpen/3secOver0.25
sbatch splitjob.sh './EMUOpen/original/*.wav' ./split.sh 3 0.5 ./EMUOpen/3secOver0.5
sbatch splitjob.sh './EMUOpen/original/*.wav' ./split.sh 3 0.75 ./EMUOpen/3secOver0.75
