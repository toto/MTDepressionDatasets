#!/bin/bash
sbatch featurejob.sh /work/shared/mhealthmqp/EMU/osfeatures/Labeled_NoMissing_2secOver0OpenSmile.csv /work/shared/mhealthmqp/EMU/osfeatures/FeatureSelected_Labeled_NoMissing_2secOver0OpenSmile.csv
sbatch featurejob.sh /work/shared/mhealthmqp/EMU/osfeatures/Labeled_NoMissing_2secOver0.25OpenSmile.csv /work/shared/mhealthmqp/EMU/osfeatures/FeatureSelected_Labeled_NoMissing_2secOver0.25OpenSmile.csv
sbatch featurejob.sh /work/shared/mhealthmqp/EMU/osfeatures/Labeled_NoMissing_2secOver0.5OpenSmile.csv /work/shared/mhealthmqp/EMU/osfeatures/FeatureSelected_Labeled_NoMissing_2secOver0.5OpenSmile.csv
sbatch featurejob.sh /work/shared/mhealthmqp/EMU/osfeatures/Labeled_NoMissing_2secOver0.75OpenSmile.csv /work/shared/mhealthmqp/EMU/osfeatures/FeatureSelected_Labeled_NoMissing_2secOver0.75OpenSmile.csv

sbatch featurejob.sh /work/shared/mhealthmqp/EMU/osfeatures/Labeled_NoMissing_3secOver0OpenSmile.csv /work/shared/mhealthmqp/EMU/osfeatures/FeatureSelected_Labeled_NoMissing_3secOver0OpenSmile.csv
sbatch featurejob.sh /work/shared/mhealthmqp/EMU/osfeatures/Labeled_NoMissing_3secOver0.25OpenSmile.csv /work/shared/mhealthmqp/EMU/osfeatures/FeatureSelected_Labeled_NoMissing_3secOver0.25OpenSmile.csv
sbatch featurejob.sh /work/shared/mhealthmqp/EMU/osfeatures/Labeled_NoMissing_3secOver0.5OpenSmile.csv /work/shared/mhealthmqp/EMU/osfeatures/FeatureSelected_Labeled_NoMissing_3secOver0.5OpenSmile.csv
sbatch featurejob.sh /work/shared/mhealthmqp/EMU/osfeatures/Labeled_NoMissing_3secOver0.75OpenSmile.csv /work/shared/mhealthmqp/EMU/osfeatures/FeatureSelected_Labeled_NoMissing_3secOver0.75OpenSmile.csv

sbatch featurejob.sh /work/shared/mhealthmqp/EMU/osfeatures/Labeled_NoMissing_originalOpenSmile.csv /work/shared/mhealthmqp/EMU/osfeatures/FeatureSelected_Labeled_NoMissing_originalOpenSmile.csv

