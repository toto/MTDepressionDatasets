import numpy as np
import pandas as pd # Pandas is used to import the CSV file
import sklearn.metrics as metrics
import math
import warnings
warnings.filterwarnings("ignore")

def clfMetrics(data,truth,prediction,score):
    #print(truth," ",prediction," ",score)
    clfMetrics = {}
    data[truth] = data[truth].astype(int)
    data[prediction] = data[prediction].astype(int)

    conf_mat = metrics.confusion_matrix(data[truth], data[prediction])
    #clfMetrics["conf_mat"] = conf_mat
    clfMetrics["TN"], clfMetrics["FP"], clfMetrics["FN"], clfMetrics["TP"] = conf_mat.ravel()
    clfMetrics["f1"] = metrics.f1_score(data[truth], data[prediction])
    clfMetrics["specificity"] = metrics.precision_score(data[truth], data[prediction])
    clfMetrics["sensitivity"] = metrics.recall_score(data[truth], data[prediction])
    clfMetrics["accuracy"] = metrics.accuracy_score(data[truth], data[prediction])
    clfMetrics["auc"] = metrics.roc_auc_score(data[truth], data[score])
    return clfMetrics

               
def regMetrics(data,truth,prediction):
    #print(truth," ",prediction)
    regMetrics = {}
    regMetrics["MAE"] = metrics.mean_absolute_error(data[truth], data[prediction])
    regMetrics["r2"] = metrics.r2_score(data[truth], data[prediction])
    regMetrics["MSE"] = metrics.mean_squared_error(data[truth], data[prediction])
    regMetrics["RMSE"] = math.sqrt(regMetrics["MSE"])
    return regMetrics

def prettyPrintKeys(metricsDictionary,printHeader,csvdelim=","):
    for metric in metricsDictionary:
        print(str(metric)+csvdelim,end='')


def prettyPrint(metricsDictionary,printHeader,csvdelim=","):
    floatMetrics = ["f1","specificity","sensitivity","accuracy","auc","MAE","r2","MSE","RMSE"]
    
    for metric in metricsDictionary:
        if metric in floatMetrics:
            print("{:.{}f}".format(metricsDictionary[metric],3)+csvdelim,end="")
        else:
            print(str(metricsDictionary[metric])+csvdelim,end="")
        


def dataToCSVHelper(data,filename,grouping,cutoff,weight,aggregation,isfeaturesel,printHeader):
    if(printHeader):
        if isfeaturesel:
            print("Dataset,Model,Threshold,NumFeatures,grouping,weight,",end='') 
        else:
            print("Dataset,Model,FeatureSelection,CLFCutoff,Cutoff,Sampling,Spliting,grouping,weight,aggregation,",end='')   

        clfBinaryMetrics  = clfMetrics(data,"binaryTruth","binaryPrediction","probabilityOfOne")
        prettyPrintKeys(clfBinaryMetrics,printHeader)
        data["regresionclassification"]    = np.where(data["regressionPrediction"] > cutoff, 1, 0)
        clfRegMetrics     = clfMetrics(data,"binaryTruth","regresionclassification","probabilityOfOne")
        prettyPrintKeys(clfRegMetrics,printHeader)
        regressionMetrics = regMetrics(data,"regressionTruth","regressionPrediction")
        prettyPrintKeys(regressionMetrics,printHeader)  
        print('')
   

    if isfeaturesel:
        print(filename[0]+","+filename[1]+","+str(filename[5])+","+str(filename[6])+","+grouping+","+"{:.{}f}".format(weight,3)+",",end='')    
    else:
        print(filename[0]+","+filename[2]+","+filename[5]+","+str(filename[3])+","+str(cutoff)+","+str(filename[4])+","+str(filename[8])+","+grouping+","+"{:.{}f}".format(weight,3)+","+aggregation+",",end='')
    clfBinaryMetrics  = clfMetrics(data,"binaryTruth","binaryPrediction","probabilityOfOne")
    prettyPrint(clfBinaryMetrics,printHeader)
    data["regresionclassification"]    = np.where(data["regressionPrediction"] > cutoff, 1, 0)
    clfRegMetrics     = clfMetrics(data,"binaryTruth","regresionclassification","probabilityOfOne")
    prettyPrint(clfRegMetrics,printHeader)
    regressionMetrics = regMetrics(data,"regressionTruth","regressionPrediction")
    prettyPrint(regressionMetrics,printHeader)
    print('')

