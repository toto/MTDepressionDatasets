#!/bin/bash
echo Adding labels using ${@: -1}
for filename in $*; do
	if [ "$filename" != "${@: -1}" ]; then
		echo $filename
		awk -F"," -v OFS=',' 'NR==FNR{temp=$1;$1="";a[temp]=$0;next} a[$1]{print $0 a[$1]}' "${@: -1}" "$filename" > "Labeled_$filename"
	fi
done
