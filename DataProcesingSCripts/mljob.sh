#!/bin/bash
#SBATCH --job-name=MLJob
#SBATCH --mem=9Gb
#SBATCH -n 8
#SBATCH -t 12:00:00

#!/bin/bash

# Iterate the string array using for loop
#ParticipantID,Dataset,ConfigFile,Filename,OpenSmilePath
for cutoff in 9 10 11; do
    for model in kNN RF SVM; do
	echo Model $model Cutoff $cutoff Resample $4 Filename $1 
        python analyzerExperiments.py --cutoff $cutoff --dataStart $2 --dataEnd $3 --filename $1 --resampleType $4 --modelType $model --identifierList '{"ParticipantID": 0, "Dataset":1,"Filename": 3}' --foldcache $5 --resultsfile "$(dirname $1)/results/results_${model}_${cutoff}_${4}_$(basename $1)"
   done
done
