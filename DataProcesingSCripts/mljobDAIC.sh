#!/bin/bash
#SBATCH --job-name=MLJob
#SBATCH --mem=9Gb
#SBATCH -n 8
#SBATCH -t 12:00:00

# Iterate the string array using for loop
counter=0
model=SVM
for cutoff in 9 10 11; do
    for model in kNN RF SVM; do
	echo Counter $counter Model $model Cutoff $cutoff Resample $4 Filename $1 
        python analyzerExperiments.py --cutoff $6 --dataStart $2 --dataEnd $3 --filename $1 --resampleType $4 --modelType $model --identifierList '{"ParticipantID": 0, "QuestionNumber": 2,"Dataset":3,"Filename": 5}' --foldcache $5 --resultsfile "$(dirname $1)/results/results_${model}_${cutoff}_${4}_$(basename $1)"
	#if [ "$counter" = "3" ]; then
	#	echo Wait For ML Job
	#	wait
	#	counter=0 
	#else 
	#	echo Don\'t wait for ML Job
	#fi
	#let "counter+=1"
   done
done
