#!/bin/bash
#SBATCH --job-name=Split Files
#SBATCH --mem=4Gb
#SBATCH -N 1
#SBATCH -n 10
INFOLDER="$1"
CLIPPER_SCRIPT="$2"
CLIP_LENGTH="$3"
OVERLAP="$4"
OUTDIR="$5"

echo $@
date;hostname;pwd
mkdir -p "$OUTDIR"
find $INFOLDER -exec "$CLIPPER_SCRIPT" {} "$CLIP_LENGTH" "$OVERLAP" "$OUTDIR" \;
#parallel -n $SLURM_CPUS_ON_NODE "$CLIPPER_SCRIPT" {} "$CLIP_LENGTH" "$OVERLAP" "$OUTDIR" ::: $INFOLDER

