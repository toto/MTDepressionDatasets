#Author: Ermal Toto
# -*- coding: utf-8 -*-
import argparse
import random 
import json
import analyzerTools as at
import numpy as np
import pandas as pd # Pandas is used to import the CSV file
import operator
#sklearn is used for ML algorithms
#from sklearn.model_selection import cross_validate
#from sklearn.model_selection import cross_val_predict
print("STARTING ANALYZER")
#Default Configuratin Values
random.seed(123)
cutoff = 10
targetData = -1 #last feature used as class variable. 
targetDataCount = 1
resampleType = "down" #up, down
svckernel = "poly" # ‘linear’, ‘poly’, ‘rbf’, ‘sigmoid’, ‘precomputed’ or a callable. If none is given: ‘rbf’
missingValues = -999 #Remove Instances: Remove, 0, -100, -1
modelType = "RF" #SVC, RF, kNN
numNeighbors = 6 # k for kNN (number of neighbours)
gridParams = "None"
optimizeFor = "f1"
identifierList = '{"ParticipantID": 0, "QuestionNumber": 2,"Dataset":3,"Filename": 5}'
#-------------------------

#Begin Command Line Argument Parsing
parser = argparse.ArgumentParser()

parser.add_argument("--cutoff", type=int, help="Target Variable cutoff for binary classification")
parser.add_argument("--dataStart", type=int, help="Feature Subset Start Index")
parser.add_argument("--dataEnd", type=int, help="Feature Subset End Index")
parser.add_argument("--filename", help="Input filename, default: mtrComplete")
parser.add_argument("--resultsfile", help="Results output filename")
parser.add_argument("--resampleType", help="Resample Type, default: downsample", choices=['up','down','None'])
parser.add_argument("--svckernel", help="Support Vector Classifier Kernel, default: rbf", choices=['linear', 'poly', 'rbf', 'sigmoid', 'precomputed'])
parser.add_argument("--modelType", help="Classifier Type, default: RF", choices=['SVM', 'RF', 'kNN','GP'])
parser.add_argument("--numNeighbors", type=int, help="Number of neighbours for kNN, default: 3")
parser.add_argument("--targetDataCount", type=int, help="Number of features that are part of the target class, default is 1")
parser.add_argument("--targetData", type=int, help="Location of target data. Default -1. This should always be negative")
parser.add_argument("--missingValues", type=int, help="what to replace missing values with -999 will remove them")
parser.add_argument("--gridParams",  help="Parameter Scope for ML Models")
parser.add_argument("--optimizeFor",  help="Optimization Target, Default: f1", choices=['precision', 'recall', 'f1','accuracy'])
parser.add_argument("--foldcache", help="Fold Cache File. Default=foldchache.json")
parser.add_argument("--identifierList", help='Json list of Identifiers and indeces: Default: {"ParticipantID": 0, "QuestionNumber": 2,"Dataset":3,"Filename": 5}. The first should allways be the unique scope identifier (i.e: between participants, or between questions)')

args = parser.parse_args()
if args.__dict__["missingValues"]  is not None:
    missingValues = args.__dict__["missingValues"]
if args.__dict__["targetData"]  is not None:
    targetData = args.__dict__["targetData"]
if args.__dict__["targetDataCount"]  is not None:
    targetDataCount = args.__dict__["targetDataCount"]
if args.__dict__["cutoff"]  is not None:
    cutoff = args.__dict__["cutoff"]
if args.__dict__["dataStart"]  is not None:
    dataStart = args.__dict__["dataStart"]
if args.__dict__["dataEnd"]  is not None:
    dataEnd = args.__dict__["dataEnd"]
if args.__dict__["filename"]  is not None:
    filename = args.__dict__["filename"]
if args.__dict__["resultsfile"]  is not None:
    resultsfile = args.__dict__["resultsfile"]    
if args.__dict__["resampleType"]  is not None:
    resampleType = args.__dict__["resampleType"]
if args.__dict__["svckernel"]  is not None:
    svckernel = args.__dict__["svckernel"]
if args.__dict__["modelType"]  is not None:
    modelType = args.__dict__["modelType"]
if args.__dict__["numNeighbors"]  is not None:
    numNeighbors = args.__dict__["numNeighbors"]
if args.__dict__["gridParams"]  is not None:
    gridParams  = args.__dict__["gridParams"]
if args.__dict__["optimizeFor"]  is not None:
    optimizeFor  = args.__dict__["optimizeFor"]
if args.__dict__["foldcache"]  is not None:
    foldcache = args.__dict__["foldcache"]
    with open(foldcache) as json_file:
        participantIdFolds = json.load(json_file)      
if args.__dict__["identifierList"]  is not None:
    identifierList = args.__dict__["identifierList"]

import os.path
if os.path.isfile(resultsfile):
    print ("Results File: " + resultsfile +" does exist")
    exit()
else:
    print ("Results File: " + resultsfile +" does not exist")
    
identifierList = json.loads(identifierList)
uniqueIdentifier = list(identifierList.keys())[0]
    
#End Command Line Argument Parsing

#Open Data File
data = pd.read_csv(filename)


if missingValues == '-999':
    data = data.dropna()
else:
    data = data.replace(np.nan, missingValues, regex=True) # Replace all missing values with missingValues as defined



#Add Binary Class
binary_classes = np.where(data[data.columns[targetData]] > cutoff, 1, 0)
data["binary_class"] = binary_classes

#Adujst for the addition of binary label
upToLabels = (targetDataCount*-1)-1
binaryLabel = targetData
numericLabel = targetData - 1

if resampleType != "None":
    data = at.resampleDataset(data,resampleType,binaryLabel)


    
from sklearn import metrics
from sklearn import svm
#from statistics import mean 
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.multiclass import OneVsRestClassifier
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import RBF
from sklearn.neural_network import MLPClassifier
from sklearn.ensemble import RandomForestRegressor
from sklearn.neighbors import KNeighborsRegressor
#from sklearn.svm import SVR



#Cross Validation will happen with predefined folds for replicability
#See script for generating folds
#CV is implemented from scratch instead of using the scikit
#Default CV to have more control of the underlying mechanism, as this might
#be required by metalgorithms such as SCB (Toto et al. 2018 ECML-PKDD)
master_y_pred = np.array(0)
master_y_score = np.array(0)
master_y_true = np.array(0)
firstFold = True

for fold in participantIdFolds:
    trainids = participantIdFolds[fold]["TRAIN"]
    testids  = participantIdFolds[fold]["TEST"]
    trainset = data[data[uniqueIdentifier].isin(trainids)]
    testset  = data[data[uniqueIdentifier].isin(testids)]
    #SVC, RF, kNN
    if modelType == "SVM":
        clf = svm.SVC(kernel=svckernel, probability=True)
        regressor = svm.SVR(kernel=svckernel)
    elif modelType == "RF":
        n_estimators=30
        clf = RandomForestClassifier(n_estimators=n_estimators, max_depth=10)
        regressor = RandomForestRegressor(n_estimators=n_estimators)
    elif modelType == "kNN":
        clf = KNeighborsClassifier(n_neighbors=numNeighbors)
        regressor = KNeighborsRegressor(n_neighbors=numNeighbors)
    elif modelType == "GP":
        kernel = 1.0 * RBF(1.0)
        clf = GaussianProcessClassifier(kernel=kernel, random_state=0)
        regressor = GaussianProcessRegressor(kernel=kernel, n_restarts_optimizer=10, alpha=0.1, normalize_y=True)

   
    #Classification Model with Binary Labels and Predictions
    clf.fit(trainset[trainset.columns[dataStart:upToLabels]], trainset[trainset.columns[binaryLabel]])
    y_pred = clf.predict(testset[testset.columns[dataStart:upToLabels]])
    y_score = clf.predict_proba(testset[testset.columns[dataStart:upToLabels]])
    #Ground Truth for Classification
    y_true = np.array(testset[testset.columns[binaryLabel]])
    
    #Regression Model and Predictions
    regressor.fit(trainset[trainset.columns[dataStart:upToLabels]], trainset[trainset.columns[numericLabel]])
    y_pred_reg = regressor.predict(testset[testset.columns[dataStart:upToLabels]]) 
    
    #Ground Truth for Regression
    y_true_numeric = np.array(testset[testset.columns[numericLabel]])
    #Combine Fold Results
    if firstFold:
        master_y_pred = y_pred
        master_y_score = y_score
        master_y_true = y_true 
        master_y_pred_reg = y_pred_reg
        master_y_true_numeric = y_true_numeric 
        master_y_testids = testset[testset.columns[:dataStart]]
    else:
        master_y_pred = np.concatenate((master_y_pred,y_pred),axis=None)
        master_y_score = np.concatenate((master_y_score,y_score),axis=0)
        master_y_pred_reg = np.concatenate((master_y_pred_reg,y_pred_reg),axis=None)
        master_y_true = np.concatenate((master_y_true,y_true),axis=None)
        master_y_true_numeric = np.concatenate((master_y_true_numeric,y_true_numeric),axis=None)
        master_y_testids = np.concatenate((master_y_testids,testset[testset.columns[:dataStart]]),axis=0)
    firstFold = False

isId = True
for key, value in identifierList.items():
    if isId:
        finalResults = pd.DataFrame({key:master_y_testids[:,value].tolist()})
        isId = False
    else:
        finalResults[key] = master_y_testids[:,value].tolist()
    
finalResults['binaryPrediction'] = master_y_pred.tolist()
finalResults['probabilityOfZero'] = master_y_score[:,0].tolist()
finalResults['probabilityOfOne'] = master_y_score[:,1].tolist()
finalResults['regressionPrediction'] = master_y_pred_reg.tolist()
finalResults['binaryTruth'] = master_y_true.tolist()
finalResults['regressionTruth'] = master_y_true_numeric.tolist()
finalResults.to_csv(resultsfile, sep=',')      
