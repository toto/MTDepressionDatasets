#Author: Ermal Toto
# -*- coding: utf-8 -*-
import argparse
import random 
random.seed(123)
#Default Values
verbose = False
cutoff = 10
dataStart = 1
dataEnd = 5
targetData = -1 #last feature used as class variable. 
targetDataCount = 1
folds = 5
filename = 'csvdata/textfeatures.csv'
filename2 = None
featureType = 'text'
resampleType = "down" #up, down
svckernel = "linear" # ‘linear’, ‘poly’, ‘rbf’, ‘sigmoid’, ‘precomputed’ or a callable. If none is given: ‘rbf’
scoring = ['precision', 'recall', 'f1','accuracy']
numberOfFeatures = 10
missingValues = -999 #Remove Instances: Remove, 0, -100, -1
modelType = "RF" #SVC, RF, kNN
numNeighbors = 3 # k for kNN (number of neighbours)
doFeatureSelection = "False"
printResultHeader = "True"
gridParams = "None"
optimizeFor = "f1"
#-------------------------

parser = argparse.ArgumentParser()

parser.add_argument("--cutoff", type=int, choices=[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20],
                    help="Depression Classification cutoff PHQ-9 [1-20] or Q9 cutoff [1-3]")
parser.add_argument("--dataStart", type=int, help="Feature Subset Start Index")
parser.add_argument("--dataEnd", type=int, help="Feature Subset End Index")
parser.add_argument("--filename", help="Input filename, default: mtrComplete")
parser.add_argument("--filename2", help="Input filename, default: mtrComplete")
parser.add_argument("--featureType", help="Input file feature type, default: text", choices = ['text','audio','gps'])
parser.add_argument("--resampleType", help="Resample Type, default: downsample", choices=['up','down','None'])
parser.add_argument("--svckernel", help="Support Vector Classifier Kernel, default: rbf", choices=['linear', 'poly', 'rbf', 'sigmoid', 'precomputed'])
parser.add_argument("--modelType", help="Classifier Type, default: RF", choices=['SVC', 'RF', 'kNN', 'XGB', 'ADA','Voting', 'LR', 'GP', 'NN'])
parser.add_argument("--numNeighbors", type=int, help="Number of neighbours for kNN, default: 3")
parser.add_argument("--folds", type=int, help="Number of folds for cross validation, default: 5")
parser.add_argument("--numberOfFeatures", type=int, help="Number of features to be selected, default: 40 or difference between dataEnd and dataStart whichever is smaller")
parser.add_argument("--doFeatureSelection", choices =['True','False','PCA','LDA','QDA','RFE'], help="If false, all features are used")
parser.add_argument("--targetDataCount", type=int, help="Number of features that are part of the target class, default is 1")
parser.add_argument("--printResultHeader", help="Print header for result metrics i.e: accuracy,precision..etc")
parser.add_argument("--targetData", type=int, help="Location of target data -1 for PHQ9 and -2 for Q9")
parser.add_argument("--missingValues", type=int, help="what to replace missing values with -999 will remove them")
parser.add_argument("--gridParams",  help="Parameter Scope for ML Models")
parser.add_argument("--optimizeFor",  help="Optimization Target, Default: f1", choices=['precision', 'recall', 'f1','accuracy'])


args = parser.parse_args()
if args.__dict__["missingValues"]  is not None:
    missingValues = args.__dict__["missingValues"]
if args.__dict__["targetData"]  is not None:
    targetData = args.__dict__["targetData"]
if args.__dict__["doFeatureSelection"]  is not None:
    doFeatureSelection = args.__dict__["doFeatureSelection"]    
if args.__dict__["targetDataCount"]  is not None:
    targetDataCount = args.__dict__["targetDataCount"]
if args.__dict__["printResultHeader"]  is not None:
    printResultHeader = args.__dict__["printResultHeader"]
    
if args.__dict__["cutoff"]  is not None:
    cutoff = args.__dict__["cutoff"]
if args.__dict__["dataStart"]  is not None:
    dataStart = args.__dict__["dataStart"]
if args.__dict__["dataEnd"]  is not None:
    dataEnd = args.__dict__["dataEnd"]
if args.__dict__["filename"]  is not None:
    filename = args.__dict__["filename"]
if args.__dict__["filename2"]  is not None:
    filename2 = args.__dict__["filename2"]
if args.__dict__["featureType"]  is not None:
    featureType = args.__dict__["featureType"]
if args.__dict__["resampleType"]  is not None:
    resampleType = args.__dict__["resampleType"]
if args.__dict__["svckernel"]  is not None:
    svckernel = args.__dict__["svckernel"]
if args.__dict__["modelType"]  is not None:
    modelType = args.__dict__["modelType"]
if args.__dict__["numNeighbors"]  is not None:
    numNeighbors = args.__dict__["numNeighbors"]
if args.__dict__["folds"]  is not None:
    folds = args.__dict__["folds"]
if args.__dict__["numberOfFeatures"]  is not None:
    numberOfFeatures  = args.__dict__["numberOfFeatures"]
if args.__dict__["gridParams"]  is not None:
    gridParams  = args.__dict__["gridParams"]
if args.__dict__["optimizeFor"]  is not None:
    optimizeFor  = args.__dict__["optimizeFor"]

import numpy as np
import pandas as pd # Pandas is used to import the CSV file
import collections
import operator
if numberOfFeatures > (dataEnd - dataStart):
        numberOfFeatures = dataEnd - dataStart

# In[18]:

#print(numberOfFeatures)

data = pd.read_csv(filename)
featureSubset = data[data.columns[dataStart:dataEnd]] #Skip PHQ-9 Responses
target = data[data.columns[targetData]]
featureSubset=featureSubset.assign(target = target)

if filename2:
    data2 = pd.read_csv(filename2)
    featureSubset2 = data2[data2.columns[dataStart:dataEnd]]  # Skip PHQ-9 Responses
    target2 = data2[data2.columns[targetData]]
    featureSubset2 = featureSubset2.assign(target=target2)

if missingValues == '-999':
    featureSubset = featureSubset.dropna()
    if filename2:
        featureSubset2 = featureSubset2.dropna()
else:
    featureSubset = featureSubset.replace(np.nan, missingValues, regex=True) # Replace all missing values with missingValues as defined
    if filename2:
        featureSubset2 = featureSubset2.replace(np.nan, missingValues,regex=True)  # Replace all missing values with missingValues as defined
featureSubset[featureSubset.columns[-1]] = np.where(featureSubset[featureSubset.columns[-1]] > cutoff, 1, 0)
if filename2:
    featureSubset2[featureSubset2.columns[-1]] = np.where(featureSubset2[featureSubset2.columns[-1]] > cutoff, 1, 0)


# In[19]:

#Identify majority and miniority class for upsample/downsample proceedures
targetClassCount = collections.Counter(featureSubset[featureSubset.columns[-1]])
majorityKey = max(targetClassCount, key=targetClassCount.get)
majorityCount = targetClassCount[majorityKey]
minorityKey = min(targetClassCount,  key=targetClassCount.get)
minorityCount = targetClassCount[minorityKey]

# In[20]:


#Separate minority and majority classes
featureSubset_majority = featureSubset[featureSubset[featureSubset.columns[len(featureSubset.columns)-1]] == majorityKey]
featureSubset_minority = featureSubset[featureSubset[featureSubset.columns[len(featureSubset.columns)-1]] == minorityKey]


# In[21]:


from sklearn.utils import resample
# Upsample minority class
featureSubset_minority_upsampled = resample(featureSubset_minority, 
                                 replace=True,     # sample with replacement
                                 n_samples=majorityCount) 
 
# Combine majority class with upsampled minority class
featureSubset_upsampled = pd.concat([featureSubset_majority, featureSubset_minority_upsampled])
featureSubset_upsampled = featureSubset_upsampled.sample(frac=1).reset_index(drop=True)


# In[22]:


from sklearn.utils import resample
# Downsample minority class
featureSubset_majority_downsampled = resample(featureSubset_majority, 
                                 replace=False,     # sample with replacement
                                 n_samples=minorityCount,    # to match majority class
                                 ) 
 
# Combine majority class with upsampled minority class
featureSubset_downsampled = pd.concat([featureSubset_majority_downsampled, featureSubset_minority])
featureSubset_downsampled = featureSubset_downsampled.sample(frac=1).reset_index(drop=True)


# In[23]:
if resampleType == "up":
    featureSubset = featureSubset_upsampled
elif resampleType == "down":
    featureSubset = featureSubset_downsampled


# In[24]:
# separate target from features
target = featureSubset[featureSubset.columns[-1]]
featureSubset = featureSubset[featureSubset.columns[:targetDataCount*-1]] #Skip PHQ-9 Responses

if filename2:
    target2 = featureSubset2[featureSubset2.columns[-1]]
    featureSubset2 = featureSubset2[featureSubset2.columns[:targetDataCount * -1]]


# In[25]: Scale date between 0 and 1. Several algorithms including feature selection need this. 
# import pandas as pd
# from sklearn import preprocessing
# min_max_scaler = preprocessing.MinMaxScaler()
# np_scaled = min_max_scaler.fit_transform(featureSubset)
# featureSubset = pd.DataFrame(np_scaled)

# feature reduction
# from sklearn.decomposition import PCA
# pca = PCA(n_components = 2,svd_solver= 'auto')
# pca.fit(X=featureSubset_downsampled)


# In[26]: Feature Selection
if(doFeatureSelection == "True"):
    from sklearn.datasets import load_digits
    from sklearn.feature_selection import SelectKBest, chi2
    featureSubset = SelectKBest(chi2, k=numberOfFeatures).fit_transform(featureSubset, target)


if(doFeatureSelection == "PCA"):
    from sklearn.datasets import load_digits
    from sklearn.decomposition import PCA
    pca = PCA(n_components = numberOfFeatures,svd_solver= 'auto')
    pca.fit(X = featureSubset)
    pca.transform(X = featureSubset)

if(doFeatureSelection == "LDA"):
    from sklearn.datasets import load_digits
    from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
    lda = LinearDiscriminantAnalysis(n_components = numberOfFeatures)
    lda.fit(X=featureSubset, y=target)
    lda.transform(X=featureSubset)


if(doFeatureSelection == "QDA"):
    from sklearn.datasets import load_digits
    from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
    qda = QuadraticDiscriminantAnalysis()
    qda.fit(X=featureSubset, y=target)


if(doFeatureSelection == "RFE"):
    from sklearn.datasets import load_digits
    from sklearn.feature_selection import RFE
    from sklearn.svm import SVR
    from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
    featureSubset = RFE(SVR(kernel="linear"), numberOfFeatures, step=1).fit_transform(featureSubset, target)






# In[33]:
#sklearn is used for ML algorithms
from sklearn.model_selection import cross_validate
from sklearn.model_selection import cross_val_predict
from sklearn.metrics import confusion_matrix
from sklearn.metrics import recall_score
from sklearn import svm
from statistics import mean 
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from xgboost import XGBClassifier
from sklearn.model_selection import GridSearchCV
from sklearn.ensemble import VotingClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.gaussian_process.kernels import RBF
from sklearn.neural_network import MLPClassifier



import json
if featureType == "text":
    if gridParams != "None":
        gridParams = json.loads(gridParams)

    #Model Selection
    if modelType == "SVC":
        clf = svm.SVC(kernel=svckernel, C=1, gamma=10)
        if gridParams != "None":
            # testgrid = {'C':[10, 1]}
            clf = GridSearchCV(estimator=svm.SVC(),param_grid= gridParams,scoring=optimizeFor,cv=folds,n_jobs=-1)
            clf.fit(X=featureSubset,y=target)

    elif modelType == "RF":
        clf = RandomForestClassifier(n_estimators=400, max_depth=3,min_samples_split=5,min_samples_leaf=1)
        if gridParams != "None":
            clf = GridSearchCV(estimator=RandomForestClassifier(), param_grid=gridParams,scoring=optimizeFor,cv=folds,n_jobs=-1)
            clf.fit(X=featureSubset, y=target)
    elif modelType == "kNN":
        clf = KNeighborsClassifier(n_neighbors=9,leaf_size=1)
        if gridParams != "None":
            clf = GridSearchCV(estimator=KNeighborsClassifier(), param_grid=gridParams,scoring=optimizeFor,cv=folds,n_jobs=-1)
            clf.fit(X=featureSubset, y=target)
    elif modelType == "XGB":
        clf = XGBClassifier(learning_rate=0.01,gamma=5, max_depth= 4, min_child_weight= 1, subsample= 0.7,colsample_bytree= 0.6)
        if gridParams != "None":
            clf = GridSearchCV(estimator=XGBClassifier(), param_grid=gridParams,scoring=optimizeFor,cv=folds,n_jobs=-1)
            clf.fit(X=featureSubset, y=target)
    elif modelType == "ADA":
        clftest = RandomForestClassifier(n_estimators=400, max_depth=3)
        clf = AdaBoostClassifier(clftest, n_estimators= 400)
        if gridParams != "None":
            clf = GridSearchCV(estimator=AdaBoostClassifier(), param_grid=gridParams,scoring=optimizeFor,cv=folds,n_jobs=-1)
    elif modelType == "LR":
        clf = LogisticRegression(C=0.1, random_state=0, solver='lbfgs', multi_class='ovr')
        if gridParams != "None":
            clf = GridSearchCV(estimator=LogisticRegression(), param_grid=gridParams,scoring=optimizeFor,cv=folds,n_jobs=-1)
            clf.fit(X=featureSubset, y=target)
    elif modelType == "GP":
        kernel = 1.0 * RBF(1.0)
        clf = GaussianProcessClassifier(kernel=kernel, random_state=1,max_iter_predict=1,n_jobs=1)
        if gridParams != "None":
            clf = GridSearchCV(estimator=GaussianProcessClassifier(kernel=kernel, random_state=0), param_grid=gridParams,scoring=optimizeFor,cv=folds,n_jobs=-1)
            clf.fit(X=featureSubset, y=target)
    elif modelType == "NN":
        clf = MLPClassifier(alpha=1,max_iter=50, learning_rate="adaptive", learning_rate_init=2,solver="sgd")
        if gridParams != "None":
            clf = GridSearchCV(estimator=MLPClassifier(alpha=1), param_grid=gridParams,scoring=optimizeFor,cv=folds,n_jobs=-1)
            clf.fit(X=featureSubset, y=target)
    elif modelType == "Voting":
        kernel = 1.0 * RBF(1.0)
        clf1 = svm.SVC(kernel=svckernel, C=1, gamma=10)
        clf2 = RandomForestClassifier(n_estimators=400, max_depth=3,min_samples_split=5,min_samples_leaf=1)
        clf3 = KNeighborsClassifier(n_neighbors=numNeighbors)
        clf4 = XGBClassifier(learning_rate=0.01,gamma=0.05, max_depth= 3, min_child_weight= 6, subsample= 0.62,colsample_bytree= 0.6)
        clf5 = AdaBoostClassifier(n_estimators=100)
        clf6 = LogisticRegression(C=10, random_state=0, solver='lbfgs', multi_class='ovr')
        clf7 = GaussianProcessClassifier(kernel=kernel, random_state=1,max_iter_predict=1,n_jobs=1)
        clf = VotingClassifier(estimators=[('RF', clf1), ('SVC', clf2), ('GP', clf7)], voting='hard')
        if gridParams != "None":
            params = {'SVC__C': [1.0, 10, 100.0, 1000], 'RF__n_estimators': [20, 200, 400]}
            clf = GridSearchCV(estimator=clf, param_grid=params,scoring=optimizeFor,cv=folds,n_jobs=-1)

    import warnings
    warnings.filterwarnings("ignore", category=DeprecationWarning)

elif featureType == "audio":
    if gridParams != "None":
        gridParams = json.loads(gridParams)

    # Model Selection
    if modelType == "SVC":
        clf = svm.SVC(kernel=svckernel, C=0.001, gamma=0.001)
        if gridParams != "None":
            # testgrid = {'C':[10, 1]}
            clf = GridSearchCV(estimator=svm.SVC(), param_grid=gridParams, scoring=optimizeFor, cv=folds, n_jobs=-1)
            clf.fit(X=featureSubset, y=target)

    elif modelType == "RF":
        clf = RandomForestClassifier(n_estimators=20, max_depth=5, min_samples_split=3, min_samples_leaf=3)
        if gridParams != "None":
            clf = GridSearchCV(estimator=RandomForestClassifier(), param_grid=gridParams, scoring=optimizeFor, cv=folds,
                               n_jobs=-1)
            clf.fit(X=featureSubset, y=target)
    elif modelType == "kNN":
        clf = KNeighborsClassifier(n_neighbors=3, leaf_size=1)
        if gridParams != "None":
            clf = GridSearchCV(estimator=KNeighborsClassifier(), param_grid=gridParams, scoring=optimizeFor, cv=folds,
                               n_jobs=-1)
            clf.fit(X=featureSubset, y=target)
    elif modelType == "XGB":
        clf = XGBClassifier(gamma=0.1, max_depth=4, min_child_weight=1, subsample=0.8,
                            colsample_bytree=0.6)
        if gridParams != "None":
            clf = GridSearchCV(estimator=XGBClassifier(), param_grid=gridParams, scoring=optimizeFor, cv=folds,
                               n_jobs=-1)
            clf.fit(X=featureSubset, y=target)
    elif modelType == "ADA":
        clftest = RandomForestClassifier(n_estimators=400, max_depth=3)
        clf = AdaBoostClassifier(clftest, n_estimators=400)
        if gridParams != "None":
            clf = GridSearchCV(estimator=AdaBoostClassifier(), param_grid=gridParams, scoring=optimizeFor, cv=folds,
                               n_jobs=-1)
    elif modelType == "LR":
        clf = LogisticRegression(C=0.01, random_state=0, solver='lbfgs', multi_class='ovr')
        if gridParams != "None":
            clf = GridSearchCV(estimator=LogisticRegression(), param_grid=gridParams, scoring=optimizeFor, cv=folds,
                               n_jobs=-1)
            clf.fit(X=featureSubset, y=target)
    elif modelType == "GP":
        kernel = 1.0 * RBF(1.0)
        clf = GaussianProcessClassifier(kernel=kernel, random_state=1, max_iter_predict=1, n_jobs=1)
        if gridParams != "None":
            clf = GridSearchCV(estimator=GaussianProcessClassifier(kernel=kernel, random_state=0),
                               param_grid=gridParams, scoring=optimizeFor, cv=folds, n_jobs=-1)
            clf.fit(X=featureSubset, y=target)
    elif modelType == "NN":
        clf = MLPClassifier(alpha=1, max_iter=50, learning_rate="constant", learning_rate_init=0.001, solver="sgd")
        if gridParams != "None":
            clf = GridSearchCV(estimator=MLPClassifier(alpha=1), param_grid=gridParams, scoring=optimizeFor, cv=folds,
                               n_jobs=-1)
            clf.fit(X=featureSubset, y=target)
    elif modelType == "Voting":
        clf1 = svm.SVC(kernel=svckernel)
        clf2 = RandomForestClassifier(n_estimators=400, max_depth=3)
        clf3 = KNeighborsClassifier(n_neighbors=numNeighbors)
        clf4 = XGBClassifier(learning_rate=0.01, gamma=0.05, max_depth=3, min_child_weight=6, subsample=0.62,
                             colsample_bytree=0.6)
        clf5 = AdaBoostClassifier(n_estimators=100)
        clf6 = LogisticRegression(C=10, random_state=0, solver='lbfgs', multi_class='ovr')
        clf = VotingClassifier(estimators=[('SVC', clf1), ('RF', clf2), ('LR', clf6)], voting='hard')
        if gridParams != "None":
            params = {'SVC__C': [1.0, 10, 100.0, 1000], 'RF__n_estimators': [20, 200, 400]}
            clf = GridSearchCV(estimator=clf, param_grid=params, scoring=optimizeFor, cv=folds, n_jobs=-1)

    import warnings

    warnings.filterwarnings("ignore", category=DeprecationWarning)

elif featureType == "gps":
    if gridParams != "None":
        gridParams = json.loads(gridParams)

    # Model Selection
    if modelType == "SVC":
        clf = svm.SVC(kernel=svckernel, C=0.001, gamma=0.001)
        if gridParams != "None":
            # testgrid = {'C':[10, 1]}
            clf = GridSearchCV(estimator=svm.SVC(), param_grid=gridParams, scoring=optimizeFor, cv=folds, n_jobs=-1)
            clf.fit(X=featureSubset, y=target)

    elif modelType == "RF":
        clf = RandomForestClassifier(n_estimators=500, max_depth=3, min_samples_split=2, min_samples_leaf=3)
        if gridParams != "None":
            clf = GridSearchCV(estimator=RandomForestClassifier(), param_grid=gridParams, scoring=optimizeFor, cv=folds,
                               n_jobs=-1)
            clf.fit(X=featureSubset, y=target)
    elif modelType == "kNN":
        clf = KNeighborsClassifier(n_neighbors=9, leaf_size=1)
        if gridParams != "None":
            clf = GridSearchCV(estimator=KNeighborsClassifier(), param_grid=gridParams, scoring=optimizeFor, cv=folds,
                               n_jobs=-1)
            clf.fit(X=featureSubset, y=target)
    elif modelType == "XGB":
        clf = XGBClassifier(gamma=5, max_depth=2, min_child_weight=5, subsample=0.8,
                            colsample_bytree=0.8)
        if gridParams != "None":
            clf = GridSearchCV(estimator=XGBClassifier(), param_grid=gridParams, scoring=optimizeFor, cv=folds,
                               n_jobs=-1)
            clf.fit(X=featureSubset, y=target)
    elif modelType == "ADA":
        clftest = RandomForestClassifier(n_estimators=400, max_depth=3)
        clf = AdaBoostClassifier(clftest, n_estimators=400)
        if gridParams != "None":
            clf = GridSearchCV(estimator=AdaBoostClassifier(), param_grid=gridParams, scoring=optimizeFor, cv=folds,
                               n_jobs=-1)
    elif modelType == "LR":
        clf = LogisticRegression(C=0.01, random_state=0, solver='lbfgs', multi_class='ovr')
        if gridParams != "None":
            clf = GridSearchCV(estimator=LogisticRegression(), param_grid=gridParams, scoring=optimizeFor, cv=folds,
                               n_jobs=-1)
            clf.fit(X=featureSubset, y=target)
    elif modelType == "GP":
        kernel = 1.0 * RBF(1.0)
        clf = GaussianProcessClassifier(kernel=kernel, random_state=1, max_iter_predict=1, n_jobs=1)
        if gridParams != "None":
            clf = GridSearchCV(estimator=GaussianProcessClassifier(kernel=kernel, random_state=0),
                               param_grid=gridParams, scoring=optimizeFor, cv=folds, n_jobs=-1)
            clf.fit(X=featureSubset, y=target)
    elif modelType == "NN":
        clf = MLPClassifier(alpha=1, max_iter=50, learning_rate="constant", learning_rate_init=0.001, solver="sgd")
        if gridParams != "None":
            clf = GridSearchCV(estimator=MLPClassifier(alpha=1), param_grid=gridParams, scoring=optimizeFor, cv=folds,
                               n_jobs=-1)
            clf.fit(X=featureSubset, y=target)
    elif modelType == "Voting":
        clf1 = svm.SVC(kernel=svckernel)
        clf2 = RandomForestClassifier(n_estimators=400, max_depth=3)
        clf3 = KNeighborsClassifier(n_neighbors=numNeighbors)
        clf4 = XGBClassifier(learning_rate=0.01, gamma=0.05, max_depth=3, min_child_weight=6, subsample=0.62,
                             colsample_bytree=0.6)
        clf5 = AdaBoostClassifier(n_estimators=100)
        clf6 = LogisticRegression(C=10, random_state=0, solver='lbfgs', multi_class='ovr')
        clf = VotingClassifier(estimators=[('SVC', clf1), ('RF', clf2), ('LR', clf6)], voting='hard')
        if gridParams != "None":
            params = {'SVC__C': [1.0, 10, 100.0, 1000], 'RF__n_estimators': [20, 200, 400]}
            clf = GridSearchCV(estimator=clf, param_grid=params, scoring=optimizeFor, cv=folds, n_jobs=-1)

    import warnings

    warnings.filterwarnings("ignore", category=DeprecationWarning)

# In[34]:
# if gridParams == "None":
#     scores = cross_validate(clf, featureSubset, target, scoring=scoring,cv=folds, return_train_score=False)
#     y_pred = cross_val_predict(clf, featureSubset, target, cv=folds)
#     conf_mat = confusion_matrix(target, y_pred)
#     #print(conf_mat)
#     TP = conf_mat[1][1]
#     TN = conf_mat[0][0]
#     FP = conf_mat[0][1]
#     FN = conf_mat[1][0]
#     precision = mean(scores['test_precision'])
#     sensitivity = mean(scores['test_recall'])
#     f1 = mean(scores['test_f1'])
#     accuracy = mean(scores['test_accuracy'])
#
#     if printResultHeader == "True":
#         print("filename,cutoff,targetData,resampleType,modelType,missingValues,doFeatureSelection,dataStart,dataEnd,targetDataCount,svckernel,numberOfFeatures,numNeighbors,precision,sensitivity,f1,accuracy,TP,TN,FP,FN")
#     print(filename,",",cutoff,",",targetData,",",resampleType,",",modelType,",",missingValues,",",doFeatureSelection,",",dataStart,",",dataEnd,",",targetDataCount,",",svckernel,",",numberOfFeatures,",",numNeighbors,",",precision,",",sensitivity,",",f1,",",accuracy,",",TP,",",TN,",",FP,",",FN)

if gridParams == "None":
    if filename2:
        X = featureSubset
        y = target
        clf.fit(X, y)
        pred = clf.predict(featureSubset2)

        conf_mat = confusion_matrix(target2, pred)

        TP = conf_mat[1][1]
        TN = conf_mat[0][0]
        FP = conf_mat[0][1]
        FN = conf_mat[1][0]

        precision = TP / (TP + FP)
        sensitivity = TP / (TP + FN)
        f1 = 2 * sensitivity * precision / (precision + sensitivity)
        accuracy = (TP + TN) / (TP + TN + FP + FN)

    else:
        scores = cross_validate(clf, featureSubset, target, scoring=scoring,cv=folds, return_train_score=False)
        y_pred = cross_val_predict(clf, featureSubset, target, cv=folds)
        conf_mat = confusion_matrix(target, y_pred)
        TP = conf_mat[1][1]
        TN = conf_mat[0][0]
        FP = conf_mat[0][1]
        FN = conf_mat[1][0]
        precision = mean(scores['test_precision'])
        sensitivity = mean(scores['test_recall'])
        f1 = mean(scores['test_f1'])
        accuracy = mean(scores['test_accuracy'])


    if printResultHeader == "True":
        print("filename,filename2,cutoff,targetData,resampleType,modelType,missingValues,doFeatureSelection,dataStart,dataEnd,targetDataCount,svckernel,numberOfFeatures,numNeighbors,precision,sensitivity,f1,accuracy,TP,TN,FP,FN")
    print(filename,",",filename2,",",cutoff,",",targetData,",",resampleType,",",modelType,",",missingValues,",",doFeatureSelection,",",dataStart,",",dataEnd,",",targetDataCount,",",svckernel,",",numberOfFeatures,",",numNeighbors,",",precision,",",sensitivity,",",f1,",",accuracy,",",TP,",",TN,",",FP,",",FN)


if gridParams != "None":
    # print(clf.cv_results_)
    #print(clf.best_score_)
    #print(clf.cv_results_)
    print(clf.best_params_)

