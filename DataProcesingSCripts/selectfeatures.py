# Feature Importance
from sklearn import datasets
from sklearn import metrics
from sklearn.ensemble import ExtraTreesClassifier
import argparse
import pandas as pd # Pandas is used to import the CSV file
import numpy as np
import json

#importancethreshold = 1.5
importancethreshold = 3.3
cached = True
featurecache = "featurecache.json"
targetDataCount = 1
cutoff = 10
dataStart = 8
dataEnd = -1

parser = argparse.ArgumentParser()

parser.add_argument("--cutoff", type=int, choices=[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20],
                    help="Depression Classification cutoff PHQ-9 [1-20] or Q9 cutoff [1-3]")
parser.add_argument("--dataStart", type=int, help="Feature Subset Start Index")
parser.add_argument("--dataEnd", type=int, help="Feature Subset End Index")
parser.add_argument("--filename", help="Input filename")
parser.add_argument("--outputfile", help="Output filename")
parser.add_argument("--targetDataCount", type=int, help="Number of features that are part of the target class, default is 1")
parser.add_argument("--targetData", type=int, help="Location of target data -1 for PHQ9 and -2 for Q9")
parser.add_argument("--importancethreshold", type=float, help="Importance threshold as average multiplier")
parser.add_argument("--cached", type=bool, help="Use predefined features. Default=True")
parser.add_argument("--featurecache", help="Feature Cache File. Default=featurechache.json")

args = parser.parse_args()


if args.__dict__["cutoff"]  is not None:
    cutoff = args.__dict__["cutoff"]
if args.__dict__["dataStart"]  is not None:
    dataStart = args.__dict__["dataStart"]
if args.__dict__["dataEnd"]  is not None:
    dataEnd = args.__dict__["dataEnd"]
if args.__dict__["filename"]  is not None:
    filename = args.__dict__["filename"]
if args.__dict__["targetData"]  is not None:
    targetData = args.__dict__["targetData"]
if args.__dict__["targetDataCount"]  is not None:
    targetDataCount = args.__dict__["targetDataCount"]
if args.__dict__["importancethreshold"]  is not None:
    importancethreshold = args.__dict__["importancethreshold"]
if args.__dict__["outputfile"]  is not None:
    outputfile = args.__dict__["outputfile"]    
if args.__dict__["cached"]  is not None:
    cached = args.__dict__["cached"] 
if args.__dict__["featurecache"]  is not None:
    featurecache = args.__dict__["featurecache"]   
    
data = pd.read_csv(filename)

if not cached:
    print("Generating Features")
    featureSubset = data[data.columns[dataStart:dataEnd]] #Skip PHQ-9 Responses
    target = data[data.columns[targetData]]
    target = np.where(target > cutoff, 1, 0)
    dataNames = data.columns.values 
    featureNames = featureSubset.columns.values 

    #print(np.mean(target))
    # fit an Extra Trees model to the data
    model = ExtraTreesClassifier()
    model.fit(featureSubset, target)
    # display the relative importance of each attribute
    importances = model.feature_importances_ #array with importances of each feature
    idx = np.arange(0, featureSubset.shape[1]) #create an index array, with the number of features
    features_to_keep_indeces = idx[importances > np.mean(importances)*importancethreshold] #only keep features whose importance is greater than the mean importance
    #should be about an array of size 3 (about)
    features_to_keep_names = list(dataNames[range(0,dataStart)]) + list(featureNames[features_to_keep_indeces]) + list([dataNames[-1]])
    with open(featurecache, 'w', encoding='utf-8') as json_cache_file:
        json.dump(features_to_keep_names, json_cache_file, ensure_ascii=False, indent=4)
else:
    print("Using Cached Features")
    with open(featurecache) as json_cache_file:
        features_to_keep_names = json.load(json_cache_file)
data[features_to_keep_names].to_csv(outputfile, sep=',', encoding='utf-8',index=False)

