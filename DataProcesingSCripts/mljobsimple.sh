#!/bin/bash
#SBATCH --job-name=MLJob
#SBATCH --mem=4Gb
#SBATCH -n 4
#SBATCH -t 12:00:00

#!/bin/bash

# Iterate the string array using for loop
        python analyzerExperiments.py --cutoff $5 --dataStart $2 --dataEnd $3 --filename $1 --resampleType $4 --modelType $6 --identifierList '{"ParticipantID": 0, "QuestionNumber": 2,"Dataset":3,"Filename": 5}' --foldcache foldchache.json --resultsfile "$(dirname $1)/results/results_${model}_${cutoff}_${4}_$(basename $1)" &
